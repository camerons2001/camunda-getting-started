package com.pet.camunda_guide.delegate;

import com.pet.camunda_guide.dto.CheckLsboRequestDto;
import com.pet.camunda_guide.dto.CheckLsboResponseDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CheckLsboDelegate implements JavaDelegate {

  static String ERROR_MSG = "Error while getting data from remote service with status code: %s";
  static String PINFL = "30101625840020";
  static String IS_SERVICE_DOWN = "is_service_down";
  static String HAS_LSBO = "has_lsbo";

  RestTemplate restTemplate;

  @Override
  public void execute(DelegateExecution delegateExecution) throws Exception {
    boolean isDown = false;

    final CheckLsboRequestDto requestDto = CheckLsboRequestDto.builder()
      .pinfl(PINFL)
      .build();

    final ResponseEntity<CheckLsboResponseDto> response = doRequest(requestDto);

    if (!response.getStatusCode().is2xxSuccessful()) {
      isDown = true;
      log.error(String.format(ERROR_MSG, response.getStatusCode().value()));
    }

    delegateExecution.setVariable(IS_SERVICE_DOWN, isDown);
    delegateExecution.setVariable(HAS_LSBO, Objects.requireNonNull(response.getBody()).isStatus());
  }

  private String getUrl() {
    return UriComponentsBuilder
      .newInstance()
      .scheme("http")
      .host("10.8.71.22")
      .port(7075)
      .path("/solfy/lsbo")
      .toUriString();
  }

  private ResponseEntity<CheckLsboResponseDto> doRequest(final CheckLsboRequestDto requestDto) {
    final String url = getUrl();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    final HttpEntity<CheckLsboRequestDto> httpEntity = new HttpEntity<>(requestDto, headers);
    return restTemplate.postForEntity(url, httpEntity, CheckLsboResponseDto.class);
  }
}
