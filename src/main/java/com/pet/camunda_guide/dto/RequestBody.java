package com.pet.camunda_guide.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class RequestBody {
  @JsonProperty("id")
  private String id;

  @JsonProperty("rawResponse")
  private RawResponse rawResponse;

  @Data
  public static class RawResponse {
    @JsonProperty("hits")
    private Hits hits;

    @Data
    public static class Hits {
      @JsonProperty("hits")
      private List<Hit> hits;

      @Data
      public static class Hit {
        @JsonProperty("_source")
        private Source source;

        @Data
        public static class Source {
          @JsonProperty("message")
          private String message;
        }
      }
    }
  }
}

