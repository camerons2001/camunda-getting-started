package com.pet.camunda_guide;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pet.camunda_guide.dto.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class CamundaGettingStartedApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(CamundaGettingStartedApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    String inputFile = "E:\\input.txt"; // Path to the input file
    String outputFile = "output.txt"; // Path to the output file

    List<String> contractIds = new ArrayList<>();
    List<String> clientIds = new ArrayList<>();
    List<String> dateBegins = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
      String line;
      StringBuilder jsonInput = new StringBuilder();
      while ((line = br.readLine()) != null) {
        jsonInput.append(line);
      }

      JSONObject jsonObject = new JSONObject(jsonInput.toString());
      JSONArray hitsArray = jsonObject.getJSONObject("rawResponse").getJSONObject("hits").getJSONArray("hits");

      for (int i = 0; i < hitsArray.length(); i++) {
        JSONObject hitObject = hitsArray.getJSONObject(i);
        String message = hitObject.getJSONObject("fields").getJSONArray("message").getString(0);

        // Parse contractId, clientId, and dateBegin from the message
        String contractId = parseValue(message, "contractId");
        String clientId = parseValue(message, "clientId");
        String dateBegin = parseValue(message, "dateBegin");

        if (contractId != null && clientId != null && dateBegin != null) {
          contractIds.add(contractId);
          clientIds.add(clientId);
          dateBegins.add(dateBegin);
        }
      }

      // Write the results to output.txt
      try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))) {
        for (int i = 0; i < contractIds.size(); i++) {
          bw.write("Contract ID: " + contractIds.get(i) + "\n");
          bw.write("Client ID: " + clientIds.get(i) + "\n");
          bw.write("Date Begin: " + dateBegins.get(i) + "\n");
          bw.write("--------\n");
        }
        System.out.println("Results written to output.txt");
      } catch (IOException e) {
        e.printStackTrace();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  // Parse value for the given key from the message
  private static String parseValue(String message, String key) {
    String[] parts = message.split("\"" + key + "\":\"");

    if (parts.length > 1) {
      String[] valueParts = parts[1].split("\"");
      return valueParts[0];
    }

    return null;
  }
}
